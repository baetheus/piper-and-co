# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.5](https://gitlab.com/baetheus/piper-and-co/compare/v0.0.4...v0.0.5) (2019-11-13)


### Bug Fixes

* ci/cd typos ftl ([b62880e](https://gitlab.com/baetheus/piper-and-co/commit/b62880ed4669191061fc10dbec3e645fe1331085))

### [0.0.4](https://gitlab.com/baetheus/piper-and-co/compare/v0.0.3...v0.0.4) (2019-11-13)


### Bug Fixes

* more ci/cd nonsense ([c356294](https://gitlab.com/baetheus/piper-and-co/commit/c3562940122eae1e55242b33b0987924494a979b))

### [0.0.3](https://gitlab.com/baetheus/piper-and-co/compare/v0.0.2...v0.0.3) (2019-11-13)


### Bug Fixes

* ci/cd is hrd ([9918422](https://gitlab.com/baetheus/piper-and-co/commit/9918422a9f540730119a7219d660934aaa35cdec))

### [0.0.2](https://gitlab.com/baetheus/piper-and-co/compare/v0.0.1...v0.0.2) (2019-11-13)


### Features

* working on getting firebase functions to work ([97b4602](https://gitlab.com/baetheus/piper-and-co/commit/97b460233a7ed802c6ef37153dc1d38f556828e2))


### Bug Fixes

* nesting hosting wasn't going to work ([e2422ab](https://gitlab.com/baetheus/piper-and-co/commit/e2422ab5828a266d8142f9b8d8eb779e958821bf))
* trying out different application structures ([3241830](https://gitlab.com/baetheus/piper-and-co/commit/32418309c55df4ca055ebdfc82935fee505eb8fc))

### 0.0.1 (2019-11-13)


### Features

* initial project structure ([94744cb](https://gitlab.com/baetheus/piper-and-co/commit/94744cbfb70a0f80f2473e7bdcc45c37fb91d681))
