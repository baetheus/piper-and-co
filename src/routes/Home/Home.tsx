import { FunctionalComponent, h } from "preact";
import { Header } from "~/components/Header";

export interface HomeProps {}

/**
 * @name Home
 * @example
 * <Home />
 */
export const Home: FunctionalComponent<HomeProps> = () => {
  return (
    <main class="vw-p100 vhmn-vh100 fld-col ai-stc">
      <Header />
      <article class="fls-1-1">Content Area</article>
      <footer>Footer Area</footer>
    </main>
  );
};
