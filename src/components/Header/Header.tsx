import { FunctionalComponent, h } from "preact";

export interface HeaderProps {}

/**
 * @name Header
 * @example
 * <Header />
 */
export const Header: FunctionalComponent<HeaderProps> = () => {
  return (
    <header class="header-container fld-row ai-stc jc-ctr vhmn-3 ct-a0">
      <ul class="header-menu-left fld-row ai-ctr jc-ctr">
        <li class="pwy-6 pwx-5">Home</li>
        <li class="pwy-6 pwx-5">Products</li>
      </ul>
      <section class="header-logo pwa-6 fld-row ai-ctr jc-ctr">Logo</section>
      <ul class="header-menu-right fld-row ai-ctr jc-ctr">
        <li class="pwy-6 pwx-5">About</li>
        <li class="pwy-6 pwx-5">Cart</li>
      </ul>
    </header>
  );
};
