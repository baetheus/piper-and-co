if (process.env.NODE_ENV === "development") {
  require("preact/debug");
}

import { h, render } from "preact";
import { Router } from "preact-router";
import { Home } from "~/routes/Home";
import { NotFound } from "~/routes/NotFound";

const App = () => (
  <Router>
    <Home path="/" />
    <NotFound default />
  </Router>
);

render(<App />, document.body);
